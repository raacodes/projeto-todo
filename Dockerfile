FROM openjdk:11
ARG JAR_FILE=target/*.jar
COPY /target/*.jar projeto-todo-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "projeto-todo-0.0.1-SNAPSHOT.jar"]
EXPOSE 8080:8080